const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const app = require('express')()
const configs = require('./config')
const session = require('express-session')
const cookieParser = require('cookie-parser')


// routes
const todoRoutes = require('./routes/todo')
const userRoutes = require('./routes/user')
const passport = require('./middlewares/passport')

app.use(session({secret: configs.secret, resave: false, saveUninitialized: true}))
app.use(cookieParser())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(passport.initialize())
app.use(passport.session())

mongoose.connect(configs.mongoURI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
.then(() => console.log("Connected to mongoDb"))
.catch(error => console.log({"Something happened": error}))

app.use(function(req, res, next) {
    res.setHeader('access-control-allow-origin', "*") // mis en place du controle sur l'origin du serveur qui fait la demande à l'api
    res.header('Content-Type', 'application/json;charset=UTF-8')
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next()
})

app.use('/todo', todoRoutes)
app.use('/user', userRoutes)

app.listen(configs.PORT, function() {
    console.log(`Server listening in port ${configs.PORT}`);
})