const mongoose = require('mongoose')

const user = new mongoose.Schema({
    providerId: {type: Number},
    provider: {type: String}
})

module.exports = mongoose.model('provider', user)