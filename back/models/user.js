const mongoose = require('mongoose')

const user = new mongoose.Schema({
    email: {type: String, required: true},
    googleId: {type: String},
    password: {type: String, required: true},
    createdAt: {type: String, default: Date.now}
})

module.exports = mongoose.model('user', user)