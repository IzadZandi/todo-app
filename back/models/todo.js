const mongoose = require('mongoose')


const todo = new mongoose.Schema({
    description: {type: String},
    title: {type: String, require: true},
    created_at: {type: Date, default: Date.now()},
    owner: {type: String, require: true}
    // ajouter updated_at
})

module.exports = mongoose.model('todo', todo)
