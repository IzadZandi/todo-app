const jwt = require('jsonwebtoken')
const configs = require('../config')


module.exports = (req, res, next) => {
    console.log(req.cookies);
    if(req.cookies && req.cookies.token) {
        const verifiedToken = jwt.verify(req.cookies.token, configs.JWT_SECRET)
        if(verifiedToken) return next()
    }
    
    res.status(400).send('Invalid token')
}