const passport = require('passport')
const User = require('../../models/user')
const localStrategy = require('./strategies/localStrategy')
const googleStrategy = require('./strategies/googleStrategy')
const facebookStrategy = require('./strategies/facebookStrategy')

passport.serializeUser(function(user, done) {
    console.log(user, "user");
    done(null, user.id)
})

passport.deserializeUser(function(id, done) {
    console.log(id, "id");
    User.findById(id, function(err, user) {
        done(err, user)
    })
})

passport.use(localStrategy)
passport.use(googleStrategy)
passport.use(facebookStrategy)

module.exports = passport