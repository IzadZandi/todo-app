const Provider = require('../../../models/provider')
const FacebookStrategy = require('passport-facebook').Strategy;
const configs = require('../../../config')


const facebookStrategy = new FacebookStrategy({
    clientID: configs.facebook.id,
    clientSecret: configs.facebook.secret,
    callbackURL: "http://127.0.0.1:8888/user/register/facebook/callback"
  },
  function(accessToken, refreshToken, profile, done) {
      console.log(profile);
    Provider.findOne({ providerId: profile.id }, function (err, user) {
        if(!user) {
            const gUser = new Provider({providerId: profile.id, provider: "google"})
            gUser.save()
            .then(createdGoogleUser => done(null, createdGoogleUser))
            .catch(err => done(null, false, {message: err}))
        }else {
            done(null, user)
        }
    });
  }
)

module.exports = facebookStrategy