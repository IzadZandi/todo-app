const Provider = require('../../../models/provider')
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const configs = require('../../../config')


const gStrategy = new GoogleStrategy({
    clientID: configs.google.client,
    clientSecret: configs.google.secret,
    callbackURL: "http://127.0.0.1:8888/user/register/google/callback"
},
function(accessToken, refreshToken, profile, done) {
    Provider.findOne({ providerId: profile.id }, function (err, user) {
        if(!user) {
            const gUser = new Provider({providerId: profile.id, provider: "google"})
            gUser.save()
            .then(createdGoogleUser => done(null, createdGoogleUser))
            .catch(err => done(null, false, {message: err}))
        }else {
            done(null, user)
        }
    });
}
)

module.exports = gStrategy