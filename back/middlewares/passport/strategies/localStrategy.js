const LocalStrategy = require('passport-local').Strategy
const User = require('../../../models/user')
const bcrypt = require('bcrypt')

const localStrat = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
    },
    function(email, password, done) {
        User.findOne({email: email}, function(err, user) {
            if(err) return done(err)
            if(!user) return done(null, false, {message: "Incorrect email"})
            bcrypt.compare(password, user.password)
            .then(hash => {
                if(!hash) {
                    return done(null, false, {message: "Incorrect password"})
                }else {
                    return done(null, {email: user.email, createdAt: user.createdAt, id: user._id})
                }
            })
            .catch(err => done(err))            
        })
    }
)


module.exports = localStrat