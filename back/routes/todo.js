const route = require('express').Router()
const todoCtrl = require('../controlleurs/todo')


route.get("/", todoCtrl.getAllTodo)
route.get("/:id", todoCtrl.getOneTodo)
route.post("/", todoCtrl.createTodo)
route.put("/:id", todoCtrl.modifyTodo)
route.delete("/:id", todoCtrl.deleteTodo)

module.exports = route