const route = require('express').Router()
const userCtrl = require('../controlleurs/user')
const isAuthenticated = require('../middlewares/isAuthenticated')
const passport = require('../middlewares/passport')


route.get("/:id", isAuthenticated, userCtrl.getUserInfo)
route.get("/register/google",  passport.authenticate('google', { scope: ['profile', 'email'] }))
route.get("/register/google/callback",  userCtrl.loginGoogle)
route.get("/register/facebook",  passport.authenticate('facebook', { scope: 'read_stream' }))
route.get("/register/facebook/callback",  userCtrl.loginFacebook)
route.post("/register", userCtrl.register)
route.post("/login", userCtrl.login)
route.get("/logout", userCtrl.logout)
route.put("/:id", userCtrl.updateUser)
route.delete("/:id", userCtrl.deleteUser)

module.exports = route