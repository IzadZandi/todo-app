const todoModel = require('../models/todo')

exports.createTodo = (req,res) => {
    if(!req.body || !req.body.title || !req.body.description || !req.body.owner) return res.status(400).send('No data or body was sent !')
    const todo = {
        ...req.body
    }
    const newTodo = new todoModel(todo)
    newTodo.save(function(err, todo) {
        if(err) return res.status(500).send('An error occured, please notify the admin !')
        res.status(201).json({todo: todo})
    })
}

exports.getAllTodo = (req,res) => {
    todoModel.find({}, function(err, todos) {
        if(err) return res.status(500).send('An error occured, please notify the admin !')
        res.status(200).json({todos: todos})
    })
}

exports.getOneTodo = (req,res) => {
    const {id} = req.params
    // if(!id) return res.status(400).send('Id missing')
    todoModel.findById(id, function(err, todo) {
        if(err) return res.status(500).send('An error occured, please notify the admin !')
        res.status(200).json({todo: todo})
    })
}

exports.modifyTodo = (req,res) => {
    if(!req.body || !req.body.title || !req.body.description || !req.body.owner) return res.status(400).send('No data or body was sent !')
    const id = req.params.id
    const {title, description, owner} = req.body
    const todo = {
        title,
        description,
        owner
    }
    // if(!id) return res.status(400).send('Id missing')
    todoModel.findOneAndUpdate({_id: id}, todo, {new: true}, function(err, todo) {
        if(err) return res.status(500).send('An error occured, please notify the admin !')
        res.status(201).json({todo: todo})
    })
}

exports.deleteTodo = (req,res) => {
    const {id} = req.params
    // if(!id) return res.status(400).send('Id missing')
    todoModel.deleteOne({_id: id}, function(err) {
        if(err) return res.status(500).send('An error occured, please notify the admin !')
        res.status(200).send('todo deleted')
    })
}