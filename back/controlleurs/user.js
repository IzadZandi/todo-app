const bcrypt = require('bcrypt')
const User = require('../models/user')
const passport = require('../middlewares/passport');
const jwt = require('jsonwebtoken')
const configs = require('../config')


exports.getUserInfo = (req, res) => {
    console.log(req.user);
    res.end()
}

exports.register = (req, res) => {
    if(!req.body || !req.body.email || !req.body.password) return res.status(400).send('No data has been sent.')
    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        const user = {
            ...req.body,
            password: hash
        }
        const newUser = new User(user)
        newUser.save()
        .then(createdUser => res.status(201).json(createdUser))
        .catch(err => console.log(err))
    })
    .catch(err => res.status(500).send('Error hanshin password'))
}

exports.login = (req, res) => {
    passport.authenticate('local', function(error, user, info) {
        console.log(user);
        if(error) return res.status(500).send(error)
        if(info) return res.status(422).json(info)
        if(user) {
            const token = jwt.sign({id: user.id}, configs.JWT_SECRET)
            res.cookie('token', token, {maxAge: 900000})
            res.status(200).end()
        }
    })(req, res)
}

exports.loginGoogle = (req, res) => {
    passport.authenticate('google', function(error, user, info) {
        if(error) return res.status(500).send(error)
        if(Object.entries(info).length > 0) return res.status(422).send(info)
        if(user) res.redirect(`http://localhost:3000/dashboard?user=${user.id}`)
    })(req, res)
}

exports.loginFacebook = (req, res) => {
    passport.authenticate('facebook', function(error, user, info) {
        if(error) return res.status(500).send(error)
        if(Object.entries(info).length > 0) return res.status(422).send(info)
        if(user) res.redirect(`http://localhost:3000/dashboard?user=${user.id}`)
    })(req, res)
}

exports.logout = (req, res) => {
    req.logOut()
    console.log(req.user, "test");
    res.redirect("http://localhost:3000/login")
}

exports.updateUser = (req, res) => {
    
}

exports.deleteUser = (req, res) => {
    
}