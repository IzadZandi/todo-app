import { makeStyles } from '@material-ui/core/styles';
import fontawesome from '@fortawesome/fontawesome'
import {fab} from '@fortawesome/free-brands-svg-icons'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {ThemeProvider} from '@material-ui/styles'

import theme from './components/ui/Theme'
import Inscription from './components/Inscription'
import Login from './components/Login'
import Header from './components/ui/Header'

// import coffeeTodo from './img/coffee-and-to-do-list.jpg'
fontawesome.library.add(fab)

const useStyles = makeStyles(theme => ({
  marginDiv: {
    ...theme.mixins.toolbar
  }
}));

export default function App() {
  const classes = useStyles()

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Header />
        <div className={classes.marginDiv}></div>
        <Switch>
          <Route exact path="/" component={Inscription} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/dashboard">
            <section className="App-register">
              Dashboard
            </section>
          </Route>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}