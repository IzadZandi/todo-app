import React from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withRouter } from 'react-router-dom'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'
import { Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles(theme => ({
    cardAction: {
        justifyContent: "center"
    },
    card: {
        marginLeft: "15%",
        marginRight: "15%",
        [theme.breakpoints.down('sm')]: {
            margin: "0"
        }
    },
    container: {
        height: "100vh"
    },
    formControl: {
        width: "50%",
        [theme.breakpoints.down('sm')]: {
            width: "100%"
        }
    }
}))


function Inscription(props) {

    const classes = useStyles()

    const handleSubmit = (values) => {
        console.log("in submit");
        axios.post("http://127.0.0.1:8888/user/login", values)
            .then(response => {
                console.log(response);
                if (response.status === 200) {
                    props.history.push('/dashboard')
                }
            })
            .catch(err => alert(err))
    }

    const formik = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email("Email au mauvais format.")
                .required('Ce champs est requis !'),
            password: Yup.string()
                .required('Ce champs est requis !')
        }),
        onSubmit: handleSubmit
    })

    return (
        <Grid container justify="center" direction="column" align="center" classes={{ root: classes.container }}>
            <Grid item>
                <Card className={classes.card}>
                    <CardContent>
                        <form noValidate onSubmit={formik.handleSubmit}>
                            <Grid container justify="center" direction="column" spacing={5}>
                                <Grid item>
                                    <Typography variant="h5">Se connecter</Typography>
                                </Grid>
                                <Grid item>
                                    <TextField
                                        className={classes.formControl}
                                        id="email"
                                        type="email"
                                        label="Email"
                                        variant="outlined"
                                        autoComplete="email"
                                        required
                                        value={formik.values.email}
                                        onChange={formik.handleChange}
                                        error={formik.touched.email && Boolean(formik.errors.email)}
                                        helperText={formik.touched.email && formik.errors.email}
                                    />
                                </Grid>
                                <Grid item>
                                    <TextField
                                        className={classes.formControl}
                                        id="password"
                                        label="Password"
                                        type="password"
                                        variant="outlined"
                                        autoComplete="password"
                                        required
                                        value={formik.values.password}
                                        onChange={formik.handleChange}
                                        error={formik.touched.password && Boolean(formik.errors.password)}
                                        helperText={formik.touched.password && formik.errors.password}
                                    />
                                </Grid>
                                <Grid item>
                                    <Button size="large" type="submit" variant="outlined" color="primary" >Login</Button>
                                </Grid>
                            </Grid>
                        </form>
                        {/* <a href="http://127.0.0.1:8888/user/register/facebook">Login with facebook</a> // doesn't work on localhost, nedd https */}
                    </CardContent>
                    <CardActions className={classes.cardAction}>
                        {/* <Divider />Ou connecter vous avec <Divider /> */}
                        <Link href="http://127.0.0.1:8888/user/register/google"><FontAwesomeIcon icon={['fab', 'google']} /></Link>
                        {/* <Link></Link> */}
                    </CardActions>
                </Card>
            </Grid>
        </Grid>


    )
}

export default withRouter(Inscription)