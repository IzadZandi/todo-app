import React from 'react'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { withRouter } from 'react-router-dom'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'

const useStyles = makeStyles(theme => ({
    container: {
        height: "100vh"
    },
    formContainer: {
        marginLeft: "15%",
        marginRight: "15%",
        [theme.breakpoints.down('sm')]: {
            margin: "0"
        }
    }
}))


function Inscription(props) {

    const classes = useStyles()

    const handleSubmit = (values) => {
        axios.post("http://127.0.0.1:8888/user/register", values)
            .then(response => {
                console.log(response);
                if(response.status === 201) props.history.push('/login')
            })
            .catch(err => alert(err))
    }

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
            confirmPassword: ""
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email("Email au mauvais format.")
                .required('Ce champs est requis !'),
            password: Yup.string()
                .required('Ce champs est requis !'),
            confirmPassword: Yup.string()
                .required('Ce champs est requis !')
                .test(function (value) {
                    return value === this.parent.password
                })
        }),
        onSubmit: handleSubmit
    })

    return (
        <Grid container justify="center" direction="column" classes={{ root: classes.container }}>
            <Grid item>
                <Paper classes={{ root: classes.formContainer }}>
                    <form noValidate onSubmit={formik.handleSubmit}>
                        <Grid container alignItems="center" direction="column" spacing={4} >
                            <Grid item>
                                <Typography variant="h5">S'inscrire</Typography>
                            </Grid>
                            <Grid item>
                                <TextField
                                    id="email"
                                    type="email"
                                    label="Email"
                                    variant="outlined"
                                    autoComplete="email"
                                    required
                                    value={formik.values.email}
                                    onChange={formik.handleChange}
                                    error={formik.touched.email && Boolean(formik.errors.email)}
                                    helperText={formik.touched.email && formik.errors.email}
                                />
                            </Grid>
                            <Grid item>
                                <TextField
                                    id="password"
                                    label="Password"
                                    type="password"
                                    variant="outlined"
                                    autoComplete="password"
                                    required
                                    value={formik.values.password}
                                    onChange={formik.handleChange}
                                    error={formik.touched.password && Boolean(formik.errors.password)}
                                    helperText={formik.touched.password && formik.errors.password}
                                />
                            </Grid>
                            <Grid item>
                                <TextField
                                    id="confirmPassword"
                                    label="confirmPassword"
                                    type="password"
                                    variant="outlined"
                                    autoComplete="confirmPassword"
                                    required
                                    value={formik.values.confirmPassword}
                                    onChange={formik.handleChange}
                                    error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                                    helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                                />
                            </Grid>
                            <Grid item xs>
                                <Button size="large" type="submit" variant="outlined" color="primary" >Login</Button>
                            </Grid>
                            <Grid item>
                                <Link href="http://127.0.0.1:8888/user/register/google"><FontAwesomeIcon icon={['fab', 'google']} /></Link>
                                {/* <a href="http://127.0.0.1:8888/user/register/facebook">Register with facebook</a>// doesn't work on localhost, nedd https */}
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </Grid>
        </Grid>
    )

}

export default withRouter(Inscription)