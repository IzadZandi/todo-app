import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'


import AppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu'

const useStyles = makeStyles(theme => ({
  header: {
    alignItems: "flex-end"
  },
  tabs: {
    marginRight: "25px"
  },
  drawerIcon: {
    height: "50px",
    width: "50px"
  },
  drawer: {
    width: "10rem",
    backgroundColor: theme.palette.primary.main
  },
  menuText: {
    textAlign: "center",
    color: "white"
  }
}));

export default function Header() {
  const classes = useStyles()
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const theme = useTheme()
  const smDown = useMediaQuery(theme.breakpoints.down('xs'))

  const [value, setValue] = useState(0)
  const [openDrawer, setOpenDrawer] = useState(false)

  const tabChange = (e, value) => setValue(value);

  const menuNotAuth = [{ label: "Inscription", link: "/" }, { label: "Connexion", link: "/login" }]


  useEffect(() => {
    if (window.location.pathname === "/login" && value !== 1) {
      setValue(1)
    } else if (window.location.pathname === "/" && value !== 0) {
      setValue(0)
    }
  }, [value])

  const drawer = (
    <>
      <IconButton onClick={() => setOpenDrawer(true)}><MenuIcon className={classes.drawerIcon}></MenuIcon></IconButton>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        anchor="left"
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        onOpen={() => setOpenDrawer(true)}
        classes={{ paper: classes.drawer }}
      >
        <List disablePadding>
          {menuNotAuth.map(menu => (
            <ListItem key={menu.label} divider  button component={Link} to={menu.link}>
              <ListItemText onClick={() => setOpenDrawer(false)} classes={{root: classes.menuText}}>{menu.label}</ListItemText>
            </ListItem>
          ))}
        </List>
      </SwipeableDrawer>
    </>

  )

  const tabs = (
    <Tabs value={value} onChange={tabChange} className={classes.tabs}>
      {menuNotAuth.map(menu => <Tab key={menu.label} label={menu.label} component={Link} to={menu.link} />)}
    </Tabs>
  )

  return (
    <AppBar className={classes.header}>
      {smDown ? drawer : tabs}
    </AppBar>
  )
}